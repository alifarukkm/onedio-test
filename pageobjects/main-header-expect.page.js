var page = require('./page');
/**
	 * Hepsinin xpath i aynı olduğu için aynı değeri atıyorum.
 */
var xPath="/html/body/div[2]/div/div/div[1]/nav/a/div/h1";

var headerExpectPage = Object.create(page, {
    /**
     * define elements
     */
	//Bu kısı kod temizliği açısından kısaltılabilir tek bir değişkene
	//bütün elementler atana bilir aynı xPath e sahip oldukları için.
	exTest:		{ get : function () { return $(xPath); } },
	exVideo:	{ get : function () { return $(xPath); } },
	exFood:		{ get : function () { return $(xPath); } },
	exCafe:		{ get : function () { return $(xPath); } },
	exGundem:	{ get : function () { return $(xPath); } },
	exEmSmile:	{ get : function () { return $(xPath); } },
	exEmHeart:	{ get : function () { return $(xPath); } },
	exEmRocket:	{ get : function () { return $(xPath); } },
	exEmClap:	{ get : function () { return $(xPath); } },
	exEmSurprize:{ get : function () { return $(xPath); } }
});

module.exports = headerExpectPage;
