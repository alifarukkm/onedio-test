var page = require('./page');

var headerPage = Object.create(page, {
    /**
     * define elements
     */
    btnRemind:{ get: function () { return $('#onesignal-popover-cancel-button'); } },
    btnAd: { get : function () { return $("/html/body/div[11]/div[2]"); } },
    flashAd: { get : function () { return $(".login")}},
    flash:  { get : function () { return $(".tests"); } },
    //<button id="onesignal-popover-cancel-button" class="align-right secondary popover-button">DAHA SONRA</button>
    /**
     * define or overwrite page methods
     */
    url : { get : function () { return $(page.open.call(this,'/'))}},
    open: { value: function() {
        page.open.call(this, '/');
    } },
    
    /* submit: { value: function() {
        this.submitButton.click();
    } } */
});

module.exports = headerPage;
