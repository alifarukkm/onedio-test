var page = require('./page');

var formPage = Object.create(page, {
    /**
     * define elements
     */
    loginButton: { get: function () { return browser.element('.login'); } },
    loginChk: {get: function() {return '/html/body/nav[1]/div/section[2]/section/img'}},
    loginAreaChk: {get: function() {return $('.onedioLogin h2')}},
    username: { get: function () { return $('/html/body/div[12]/div[1]/div[2]/form/div[1]/input[1]'); } },
    password: { get: function () { return $('/html/body/div[12]/div[1]/div[2]/form/div[1]/input[2]'); } },
    
    submitButton: {
      get: function () { return $('/html/body/div[12]/div[1]/div[2]/form/button'); }
    },
    /**
     * define or overwrite page methods
     */
    avatarForVisible : { value : function(val, time){
        browser.waitForVisible(val,time);
    } },
    open: { value: function() {
        page.open.call(this, '/');
    } },
    
    submit: { value: function() {
        this.submitButton.click();
    } },
    urlUser: {value: function(){
        page.open.call(this,'/profil/');
    } }
});

module.exports = formPage;
