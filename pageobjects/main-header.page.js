var page = require('./page');

var formPage = Object.create(page, {
    /**
     * Anasayfada bulunan header elementleri
     */
    btnTest:	{ get : function () { return $(".tests=TEST"); } },
    btnVideo:	{ get : function () { return $(".videos=VİDEO"); } },
    btnFood:	{ get : function () { return $(".foods=YEMEK"); } },
	btnCafe:	{ get : function () { return $(".cafe=CAFE"); } }, 
	btnGundem:	{ get : function () { return $(".gundem=GÜNDEM"); } },
	emSmile:	{ get : function () { return $(".o-emoji-smile"); } },
	emHeart:	{ get : function () { return $(".o-emoji-heart"); } },
	emRocket:	{ get : function () { return $(".o-emoji-rocket"); } },
	emClap:		{ get : function () { return $(".o-emoji-surprise"); } },
	emSurprize:	{ get : function () { return $(".o-emoji-clap"); } },
	onedioTag:	{ get : function () { return $(".logo"); } },
	/**
	 * Eğer test sitesi yavaş açılıyorsa timeout değerini arttır.
	 */
	timeOutVal:	{ get : function () { return $(30000); } },
	/**
	 * Open fonksiyonu sayfa yönlendirmek için kullanılmakta.
	 */
    open: { value: function() {
        page.open.call(this, '/');
    } },
	/**
	 * wait fonksiyonu sayfada bir elementin visible olana kadar
	 * sayfada kalmamızı sağlayan fonksiyon.
	 */
	waitOnTag: { value: function(val,time){
		browser.waitForVisible(val,time);
	}},
	/**
	 * Açılan yeni sayfada gündem buttonu check etmek için oluşturduğum
	 * switchTab buttonu
	 */
	switchTab: { value: function(tabVal){
		browser.switchTab(tabVal);
	}}
});

module.exports = formPage;
